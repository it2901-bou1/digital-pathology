# Digital Pathology
We are a team of developers writing our bachelor's thesis on the creation and implementation of new technologies in the field of digital pathology.
## Our Team
* Markus Johannes Pedersen
* Liv Elise Herstad
* Vivi Cecilie Svendsen
* Edvard Sjøborg
* Erlend Svendsbøe Høyland
* Andreas Wågø Wilson
## Project Objectives
Our team aims to address these challenges through the creation of tailored quality assurance technologies. Building upon the success of our predecessors, we will be further developing classification tools such as wizard and compare to rectify errors pertaining to variability in scanned slides. As the project progresses, we aspire to improve these tools through machine learning driven automation, culminating in a final product that functions as an asset and not a hindrance to pathologists.
## Technology/framework
#### Built with:
## Requirements
## Installation

testing jira integration
